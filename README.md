# HotSecondsIDEA
 This plugin is mainly used for hot deployment to remote server after coding. It can support hot reloading of java, xml, html, css, js and other files, and also supports hot reloading of frameworks such as spring and mybatis.
 <br>
 
 [中文文档](https://github.com/Liubsyy/HotSecondsIDEA/wiki)
 <br>
 
 
 [English Document](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/install/document.md)
 <br>

 # Download
 
 [HotSecondsServer](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/install/download_server.md)
  <br>
  
 [HotSecondsClient](https://plugins.jetbrains.com/plugin/21635-hotsecondsclient)
 <br>

 [HotSecondsProxy](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/install/proxyserver.md)
 <br>

 # HotSeconds Extension

This is a HotSeconds extension project to support more frameworks and more features.
 
 [HotSecondsExtension](https://github.com/Liubsyy/HotSecondsExtension)

 # Quick Start
 Install and open HotSeconds, then right-click to hotswap any file, let me show you hot reloading of adding fields and adding SpringMVC methods.<br><br>
![](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/img/gif/springmvc1.gif)

<br>

You can also right-click to hot reloading mybatis xml. <br><br>
![](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/img/gif/mybatis1.gif)

<br>


Multiple files modified? Try batch hot deployment.<br><br>
![](https://github.com/Liubsyy/HotSecondsIDEA/blob/master/img/gif/batchhot.gif)


